package sk.seidl.my_demo_app.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Matus Seidl
 * seidl.matus@gmail.com
 * 2018-02-07
 */
@RestController
public class BaseController {

    @GetMapping
    public ResponseEntity<String> index(){
        String msg = " INDEX";
        return new ResponseEntity<>(msg, HttpStatus.OK);
    }


    @GetMapping("/private")
    public ResponseEntity<String> authpage(){
        String msg = "Private API  , "+ SecurityContextHolder.getContext().getAuthentication().getName()+" is logged";
        return new ResponseEntity<>(msg, HttpStatus.OK);
    }

    @GetMapping("/private1")
    public ResponseEntity<String> authpage1(){
        String msg = "Private API  , 1 "+ SecurityContextHolder.getContext().getAuthentication().getName()+" is logged";
        return new ResponseEntity<>(msg, HttpStatus.OK);
    }
}
