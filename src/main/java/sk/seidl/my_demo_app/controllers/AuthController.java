package sk.seidl.my_demo_app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sk.seidl.my_demo_app.models.domain.User;
import sk.seidl.my_demo_app.models.domain.VerificationToken;
import sk.seidl.my_demo_app.models.dto.LoginRequestDto;
import sk.seidl.my_demo_app.models.dto.LoginResponseDto;
import sk.seidl.my_demo_app.models.dto.UserDto;
import sk.seidl.my_demo_app.services.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;

/**
 * @author Matus Seidl
 * seidl.matus@gmail.com
 * 2018-02-07
 */
@RestController
@RequestMapping("auth")
public class AuthController {

    @Autowired
    private UserService service;

    @PostMapping("sign-up")
    public ResponseEntity<String> signUp(@RequestBody UserDto userDto) {
        service.create(userDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("sign-up-email")
    public ResponseEntity<String> signUpWithEmail(@RequestBody UserDto userDto, HttpServletRequest request) {
        service.createUserOverEmail(userDto, request);
        return new ResponseEntity<>("Sent registration URL to " + userDto.getEmail(), HttpStatus.OK);

    }

    @GetMapping("/sign-up-email/sign-up-confirm")
    public ResponseEntity<String> registrationWithEmail(@RequestParam(value = "token") String token) {
        VerificationToken verificationToken = service.findVerificationToken(token);
        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            return new ResponseEntity<>("Token expired", HttpStatus.BAD_REQUEST);
        }
        user.setEnabled(true);
        service.updateRegUser(user);
        return new ResponseEntity<>("Registration is Completed", HttpStatus.BAD_REQUEST);
    }

    @PostMapping("login")
    public ResponseEntity<LoginResponseDto> login(@RequestBody @Valid LoginRequestDto loginRequestDto) {
        LoginResponseDto loginResponse = service.login(loginRequestDto);
        return new ResponseEntity<>(loginResponse, HttpStatus.OK);
    }


    @GetMapping("logout")
    public ResponseEntity<LoginResponseDto> logout() {
        return new ResponseEntity<>(service.logout(), HttpStatus.OK);
    }
}
