package sk.seidl.my_demo_app.services;


import sk.seidl.my_demo_app.models.domain.User;
import sk.seidl.my_demo_app.models.domain.VerificationToken;
import sk.seidl.my_demo_app.models.dto.LoginRequestDto;
import sk.seidl.my_demo_app.models.dto.LoginResponseDto;
import sk.seidl.my_demo_app.models.dto.UserDto;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Matus Seidl (5+3)
 * 2018-02-07
 */

public interface UserService  {

    User create(UserDto userDto);

    User createUserOverEmail(UserDto userDto, HttpServletRequest request);

    LoginResponseDto login(LoginRequestDto loginRequestDto);

    LoginResponseDto logout();

    void createVerificationToken(User user, String token);

    VerificationToken findVerificationToken(String token);

    void updateRegUser(User user);


}
