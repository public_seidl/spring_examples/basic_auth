package sk.seidl.my_demo_app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sk.seidl.my_demo_app.mappers.UserMapper;
import sk.seidl.my_demo_app.models.domain.User;
import sk.seidl.my_demo_app.models.domain.VerificationToken;
import sk.seidl.my_demo_app.models.dto.LoginRequestDto;
import sk.seidl.my_demo_app.models.dto.LoginResponseDto;
import sk.seidl.my_demo_app.models.dto.UserDto;
import sk.seidl.my_demo_app.register_events.OnRegistrationCompleteEvent;
import sk.seidl.my_demo_app.repositories.TokenRepository;
import sk.seidl.my_demo_app.repositories.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

/**
 * @author Matus Seidl (5+3)
 * 2018-02-07
 */
@Service
public class UserServiceImpl implements UserService {

    private static final int EXPIRATION = 60 * 24;

    private final AuthenticationManager authenticationManager;
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final TokenRepository tokenRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;


    public UserServiceImpl(AuthenticationManager authenticationManager, UserRepository repository, PasswordEncoder passwordEncoder, TokenRepository tokenRepository) {
        this.authenticationManager = authenticationManager;
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
        this.tokenRepository = tokenRepository;
    }

    @Override
    public User create(UserDto userDto) {
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        User user = UserMapper.instant.dtoToUser(userDto);
        Optional<User> chceck = repository.findUserByNickNameAndEmail(user.getNickName(), user.getEmail());
        chceck.ifPresent(tmp -> {
            throw new RuntimeException("Email or NickName is already used ");
        });
        return repository.save(user);
    }

    @Override
    public User createUserOverEmail(UserDto userDto, HttpServletRequest request) {
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        User user = UserMapper.instant.dtoToUser(userDto);
        user = repository.save(user);
        String appUrl = request.getRequestURI();
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent
                (user, request.getLocale(), appUrl));

        return user;
    }

    @Override
    public LoginResponseDto login(LoginRequestDto loginRequestDto) {
        User user = repository.findUserByEmail(loginRequestDto.getEmail());
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequestDto.getEmail(), loginRequestDto.getPass())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new LoginResponseDto(user.getEmail(), user.getNickName());
    }

    @Override
    public LoginResponseDto logout() {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = repository.findUserByEmail(name);
        LoginResponseDto response = new LoginResponseDto(user.getEmail(), user.getNickName());
        SecurityContextHolder.getContext().setAuthentication(null);
        return response;
    }

    @Override
    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);
        myToken.setExpiryDate(calculateExpiryDate());
        tokenRepository.save(myToken);
    }

    @Override
    public VerificationToken findVerificationToken(String token) {
        return tokenRepository.findVerificationTokenByToken(token);
    }

    @Override
    public void updateRegUser(User user) {
        repository.save(user);
    }


    private Date calculateExpiryDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, EXPIRATION);
        return new Date(cal.getTime().getTime());
    }
}
