package sk.seidl.my_demo_app.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sk.seidl.my_demo_app.models.domain.VerificationToken;

/**
 * @author Matus Seidl (5+3)
 * 2018-02-08
 */

@Repository
public interface TokenRepository extends CrudRepository<VerificationToken, Long> {

    VerificationToken findVerificationTokenByToken(String token);

}