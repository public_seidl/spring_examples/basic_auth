package sk.seidl.my_demo_app.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sk.seidl.my_demo_app.models.domain.User;

import java.util.Optional;

/**
 * @author Matus Seidl (5+3)
 * 2018-02-07
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findUserByNickNameAndEmail(String nicName, String email);

    User findUserByEmail(String email);

}
