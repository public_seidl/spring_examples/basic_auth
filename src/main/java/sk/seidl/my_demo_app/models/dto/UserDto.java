package sk.seidl.my_demo_app.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Matus Seidl
 * seidl.matus@gmail.com
 * 2018-02-07
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Long id;
    private String nickName;
    private String email;
    private String password;
}
