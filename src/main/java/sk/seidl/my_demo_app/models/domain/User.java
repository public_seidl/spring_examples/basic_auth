package sk.seidl.my_demo_app.models.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


/**
 * @author Matus Seidl
 * seidl.matus@gmail.com
 * 2018-02-07
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nickName;
    private String email;
    private String password;
    // if registration DONE enable = true
    private boolean enabled;

    @OneToOne(fetch = FetchType.LAZY)
    private VerificationToken verificationToken;

    @CreatedDate
    private String created;

}
