package sk.seidl.my_demo_app.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import sk.seidl.my_demo_app.models.domain.User;
import sk.seidl.my_demo_app.models.dto.UserDto;

/**
 * @author Matus Seidl
 * seidl.matus@gmail.com
 * 2018-02-07
 */
@Mapper
public interface UserMapper {

    UserMapper instant = Mappers.getMapper(UserMapper.class);

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "nickName", target = "nickName"),
            @Mapping(source = "email", target = "email"),
            @Mapping(source = "password", target = "password"),
    })
    UserDto userToDto(User user);

    @InheritInverseConfiguration
    User dtoToUser(UserDto userDto);

}
