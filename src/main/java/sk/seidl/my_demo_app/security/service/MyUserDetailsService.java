package sk.seidl.my_demo_app.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sk.seidl.my_demo_app.models.domain.User;
import sk.seidl.my_demo_app.repositories.UserRepository;

import java.util.Collections;

/**
 * @author Matus Seidl (5+3)
 * 2018-02-08
 */

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = repository.findUserByEmail(email);
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        return new org.springframework.security.core.userdetails.User(user.getEmail()
                , user.getPassword(), enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, Collections.emptyList());
    }
}
