package sk.seidl.my_demo_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyDemoAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyDemoAppApplication.class, args);
	}

}
