package sk.seidl.my_demo_app.register_events;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;
import sk.seidl.my_demo_app.models.domain.User;

import java.util.Locale;

/**
 * @author Matus Seidl
 * seidl.matus@gmail.com
 * 2018-02-07
 */

@Getter
@Setter
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private String appUrl;
    private Locale locale;
    private User user;

    public OnRegistrationCompleteEvent(
            User user, Locale locale, String appUrl) {
        super(user);

        this.user = user;
        this.locale = locale;
        this.appUrl = appUrl;
    }
}
