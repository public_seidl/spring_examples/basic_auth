# Spring Simple Auth


#### Public URL 

* http://localhost:8080  `[GET]`
* http://localhost:8080/auth/sign-up-email  `[POST]`
* http://localhost:8080/auth/sign-up        `[POST]`
* http://localhost:8080/auth/login           `[POST]`
* http://localhost:8080/auth/logout          `[GET]`
* http://localhost:8080/auth/sign-up-email/sign-up-confirm?token=15efddb7-e4c2-441b-8822-d8e4f7e5cba9  `[GET]`

#### Private URL 

* http://localhost:8080/private       `[GET]`
* http://localhost:8080/private1      `[GET]`


JSON FOR REG.
```
{
	"nickName":"Matus",
	"email": "matus@matus.com",
	"password": "test123"
}
```
JSON FOR LOGIN

```
{
	"email": "matus@matus.com",
	"pass": "test123"
}
```
RUN MySQL on Docker
```
docker run --name demo_db_mysql -e MYSQL_ROOT_PASSWORD=abc123 -e MYSQL_DATABASE=demo_db -p 3306:3306 -d mysql
```


Don't forget 
   * Instal MySQL
   * add Email and pass to application.properties